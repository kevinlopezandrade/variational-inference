# Variational Inference

This repo contains the code used to perform **variational** inference in the
following graphical model:

<img src="plots/graphical_model.png" alt="Graphical Model" width="400" height="200">


The model is implemented in PyTorch and for the logs the wandb library is
needed.

### Plots

![Architecture](plots/architecture.png)

![Samples](plots/samples.png)


### References

* Auto-Encoding Variational Bayes, Diederik P Kingma, Max Welling.
